/**
 * scheduler js
 * 
 * @date 2013/12/10
 * @author Steven Hu
 */

/**
 * get all probes from scheduler
 * and list probe info to div with id "result" in probes.html
 */
function list_probes() {
  $.getJSON("/pearl/scheduler/probes")
    .done(function(data) {
      $("#result").html("");
      if (data.length > 0) {
        var probe_html = '<table id="probesTable" border="1" cellspacing="1" align="center" class="tablesorter">'
                       + '<thead><tr class="header">'
                       + '<th>探针标识</th>'
                       //begin add by yyzhang 20140925 support show probeName
                       + '<th>探针名称</th>'
                       //end add by yyzhang 20140925 support show probeName
                       + '<th>探针版本</th>'
                       + '<th>探针类型</th>'
                       + '<th>MAC</th>'
                       + '<th>探针状态</th>'
                       + '<th>登录 IP</th>'
                       + '<th>登录时间</th>'
                       + '<th>地理位置</th>'
                       + '<th>上次退出时间</th>'
                       + '</tr></thead>';
        probe_html = probe_html + '<tbody>';
        data.forEach(function(item, idx) {
        	
        	var probeName=(item["probeName"]==undefined||item["probeName"]=="")?"--":item["probeName"];
          probe_html = probe_html + '<tr align="center">'
                     + '<td>' + item["probeId"] + '</td>'
                     //begin add by yyzhang 20140925 support show probeName
                     + '<td>' + probeName + '</td>'
                     //end add by yyzhang 20140925 support show probeName
                     + '<td>' + item["probeVer"] + '</td>'
                     + '<td>' + item["icareType"] + '</td>'
                     + '<td>' + item["mac"] + '</td>'
                     + '<td>' + item["status"] + '</td>'
                     + '<td>' + item["lastLoginIp"] + '</td>'
                     + '<td>' + item["lastLoginTime"] + '</td>';
          if(item["address"]) {
            probe_html = probe_html + '<td>' 
                       + (item["address"]["country"] ? item["address"]["country"] : '') + '-'
                       + (item["address"]["province"] ? item["address"]["province"] : '') + '-'
                       + (item["address"]["city"] ? item["address"]["city"] : '') + '-'
                       + (item["address"]["isp"] ? item["address"]["isp"] : '')
                       + '</td>';
          } else {
            probe_html = probe_html + '<td>unknown</td>';
          }
          probe_html = probe_html 
                     + '<td>' + (item["lastLogoutTime"] ? item["lastLogoutTime"] : '') + '</td>'
                     + '</tr>';
        });
        probe_html = probe_html + '</tbody></table>';
        $("#result").html(probe_html);
        $("#probesTable").tablesorter({
          sortList:[[0,0]],
          cssHeader:"header"
        });
      }
    });
}

/**
 * get scheduler version
 */
function schdl_version() {
  $.getJSON("/pearl/scheduler/version")
    .done(function(data) {
      $("#result").html("");
      var version_html = '<ul style="list-style-type:circle">'
                       + '<li>版本号: <b>' + data["version"] + '</b></li>'
                       + '<li>发布时间: <b>' + data["compiled"] + '</b></li>'
                       + '</ul>';
      $("#result").html(version_html);
    });
}

/**
 * get test task statistic info
 */
function task_stats() {
  $.getJSON("/pearl/scheduler/task/stats")
    .done(function(data) {
      $("#result").html("");
      var stats_html = '<ul style="list-style-type:circle">'
                     + '<li>接收任务总数:<b>' + data["totalRecvTaskNum"] + '</b></li>'
                     + '</ul>';
      var cnt = 0;
      for (var probe_id in data["stats"]) {
        if (cnt === 0) {
          stats_html = stats_html 
                     + '<table border="1" cellpadding="2" align="center">'
                     + '<tr style="background-color:#80FFFF">'
                     + '<th>探针标识</th>'
                     + '<th>初始状态</th>'
                     + '<th>待下发</th>'
                     + '<th>已下发</th>'
                     + '<th>正在执行</th>'
                     + '</tr>';
        }
        console.log("probe id is " + probe_id);
        stats_html = stats_html + '<tr align="center">'
                   + '<td>' + probe_id + '</td>';
        for (var probe_stat in data["stats"][probe_id]) {
          console.log("probe stat is " + probe_stat);
          stats_html = stats_html
                     + '<td>' + (data["stats"][probe_id][probe_stat] ? data["stats"][probe_id][probe_stat] : 0) + '</td>';
        }
        stats_html = stats_html + '</tr>';
        cnt += 1;
      }
      if (cnt > 0) {
        stats_html = stats_html + "</table>";
      }
      $("#result").html(stats_html);
    });
}


/**
 * 查看服务器状态
 */
function sysInfo() {
	$("#result").html("");
	$.getJSON("/msg/interface/info?method=sysInfo")
	.done(function(data) {
		var html ='';
		$.each(data, function(key, value) { 
			html=html+key + ':' + value + '<br/>';
		}); 
 	  	$("#result").html(html);  
	 });
}

