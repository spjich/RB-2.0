#!/bin/sh

JAR_FILE=scheduler-V2.0.0.6.jar
 
PID_PRO=`ps aux | grep ${JAR_FILE} | grep -v grep | awk '{print $2}'`
kill -9 ${PID_PRO}
nohup java -jar ${JAR_FILE} >/dev/null 2>&1 &
tail -10f ./logs/scheduler.log