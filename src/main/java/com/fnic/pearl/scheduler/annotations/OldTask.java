/**
 * 
 */
package com.fnic.pearl.scheduler.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 为主动测量系统中的测量任务提供 OldTask 注解
 * 使得在做 json 格式的 serialize 和 deserialize 时可以控制是否输出相关字段
 * 
 * @author Steven Hu
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OldTask {

}
