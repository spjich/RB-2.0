package com.fnic.pearl.scheduler.action;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.eclipse.jetty.servlet.ServletHandler;

import com.fnic.pearl.scheduler.util.ResponseUtil;
import com.fnic.pearl.scheduler.util.sys.SysInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * @Title:获取系统状态信息
 * @Description:
 * @Author:吉
 * @Since:2015-4-16
 * @Version:1.1.0
 */
public class MsgInterfaceInfo extends HttpServlet
{

    public static ServletHandler handler;

    private static final long serialVersionUID = 1L;

    private static final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String reqstr = request.getParameter("method");
        if (StringUtils.isBlank(reqstr))
        {
            return;
        }
        if (StringUtils.equals("sysInfo", reqstr))
        {
            sysInfo(response);
        }

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }

    /**
     * 查看服务器状态
     * 
     * @param response
     *
     */
    private void sysInfo(HttpServletResponse response)
    {
        Map<Object, Object> res = new HashMap<>();
        res.putAll(SysInfo.getCpuData());
        res.putAll(SysInfo.memory());
        res.putAll(SysInfo.getJvmMemoryData());
        ResponseUtil.send(response, gson.toJson(res));
    }
}
