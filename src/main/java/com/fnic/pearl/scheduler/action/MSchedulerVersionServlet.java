/**
 * 
 */
package com.fnic.pearl.scheduler.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fnic.pearl.scheduler.model.SchedulerVersion;

/**
 * 获取调度器的版本号
 * 
 * @author HuHaiyang
 * @date 2013年11月21日
 */
public class MSchedulerVersionServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        StringBuilder sbuf = new StringBuilder()
                .append("{\"version\":\"").append(SchedulerVersion.version).append("\",")
                .append("\"compiled\":\"").append(SchedulerVersion.compiled).append("\"}");
        response.getWriter().write(sbuf.toString());
        response.getWriter().flush();
    }
}
