/**
 * 
 */
package com.fnic.pearl.scheduler.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fnic.pearl.scheduler.constant.CondHeaderField;
import com.fnic.pearl.scheduler.dao.SchedulerStore;
import com.fnic.pearl.scheduler.model.TestTask;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 查看指定任务的详情信息
 * 
 * @author HuHaiyang
 * @date 2013年10月8日
 */
public class MTaskInfoServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static Logger LOG = Logger.getLogger(MTaskInfoServlet.class);
    private Gson gson = new GsonBuilder()
            .excludeFieldsWithoutExposeAnnotation()
            .setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String[] values = request.getParameterValues(CondHeaderField.COND_TASK_ID);
        if (values == null || values.length != 1)
        {
            LOG.warn("get - no param " + CondHeaderField.COND_TASK_ID);
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return ;
        }
        
        long taskId = 0;
        try
        {
            taskId = Long.valueOf(values[0]);
        }
        catch (NumberFormatException e)
        {
            LOG.warn("taskId format error: " + e.getMessage());
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return ;
        }
        
        TestTask tt = SchedulerStore.getInstance().getTestTask(taskId);
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        
        response.getWriter().write(gson.toJson(tt));
        response.getWriter().flush();
    }
}
