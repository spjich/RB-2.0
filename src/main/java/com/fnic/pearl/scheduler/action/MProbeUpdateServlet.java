/**
 * 
 */
package com.fnic.pearl.scheduler.action;

import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fnic.pearl.scheduler.constant.CondHeaderField;
import com.fnic.pearl.scheduler.dao.SchedulerStore;
import com.fnic.pearl.scheduler.mod.ProbeStatusManager;
import com.fnic.pearl.scheduler.model.HeartBeatResp;
import com.fnic.pearl.scheduler.model.ProbeStatus;
import com.fnic.pearl.scheduler.model.ProbeUpdate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 提供在线升级 probe 的 维护接口
 * 
 * @author HuHaiyang
 * @date 2013年10月18日
 */
public class MProbeUpdateServlet extends HttpServlet
{
    private static final Logger LOG = Logger.getLogger(MProbeUpdateServlet.class);
    private static final long serialVersionUID = 1L;
    private Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    
    /**
     * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        String[] values = request.getParameterValues(CondHeaderField.COND_PROBE_ID);
        if (values == null || values.length != 1)
        {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return ;
        }
        
        int probeId = 0;
        try
        {
            probeId = Integer.valueOf(values[0]);
        }
        catch (NumberFormatException e)
        {
            LOG.warn("probeId format error: " + e.getMessage());
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return ;
        }
        
        ProbeStatus ps = ProbeStatusManager.getInstance().get(probeId);
        if (ps == null || ps.getStatus() != ProbeStatus.S_LOGIN)
        {
            LOG.warn("probe not login: " + probeId);
            response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            return ;
        }
        
        ProbeUpdate pu;
        try
        {
            pu = gson.fromJson(new InputStreamReader(
                    request.getInputStream()), ProbeUpdate.class);
        }
        catch (Exception e)
        {
            LOG.warn("probe update parse exception - " + e.getMessage());
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return ;
        }
        
        if (pu == null)
        {
            LOG.warn("probe update is null!");
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return ;
        }
        pu.setProbeId(probeId);
        
        HeartBeatResp hbr = new HeartBeatResp(
                CondHeaderField.HB_BTYPE_PROBEUPDATE, gson.toJson(pu));
        SchedulerStore.getInstance().pushHbResp(probeId, hbr);
        
        response.setStatus(HttpServletResponse.SC_OK);
    }
}
