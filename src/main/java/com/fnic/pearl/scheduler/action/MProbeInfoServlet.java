/**
 * 
 */
package com.fnic.pearl.scheduler.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fnic.pearl.scheduler.constant.CondHeaderField;
import com.fnic.pearl.scheduler.mod.ProbeStatusManager;
import com.fnic.pearl.scheduler.model.ProbeStatus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 查看指定探针标识的探针信息
 * 
 * @author HuHaiyang
 * @date 2014年2月24日
 */
public class MProbeInfoServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private static Logger LOG = Logger.getLogger(MProbeInfoServlet.class);
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException
    {
        String[] values = req.getParameterValues(CondHeaderField.COND_PROBE_ID);
        if (values == null || values.length != 1)
        {
            LOG.warn("doPost - no param " + CondHeaderField.COND_PROBE_ID);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return ;
        }
        
        int probeId = 0;
        try
        {
            probeId = Integer.valueOf(values[0]);
        }
        catch (NumberFormatException e)
        {
            LOG.warn("probeId format error: " + e.getMessage());
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return ;
        }
        
        if (probeId <= 0)
        {
            LOG.warn("probeId = " + probeId);
            resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return ;
        }
        
        ProbeStatus ps = ProbeStatusManager.getInstance().get(probeId);
        resp.setContentType("application/json;charset=utf-8");
        resp.setStatus(HttpServletResponse.SC_OK);
        
        resp.getWriter().write(gson.toJson(ps));
        resp.getWriter().flush();
    }
    
    
}
