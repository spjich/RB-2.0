/**
 * 
 */
package com.fnic.pearl.scheduler.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fnic.pearl.scheduler.mod.ProbeStatusManager;
import com.fnic.pearl.scheduler.model.ProbeStatus;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 查看当前登录的探针列表信息
 * 
 * @author HuHaiyang
 * @date 2013年10月8日
 */
public class MProbeListServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    private Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    
    /**
     * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException
    {
        List<ProbeStatus> lstProbeStatus = ProbeStatusManager.getInstance().getAll();
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        
        response.getWriter().write(gson.toJson(lstProbeStatus));
        response.getWriter().flush();
    }
}
