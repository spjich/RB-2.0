/**
 * 
 */
package com.fnic.pearl.scheduler.model;

/**
 * 心跳响应
 * 
 * @author HuHaiyang
 * @date 2013年10月21日
 */
public class HeartBeatResp
{
    // 心跳响应体的类型
    private String hbType;
    
    // 心跳响应内容
    private String content;
    
    public HeartBeatResp()
    {
        super();
    }

    public HeartBeatResp(String hbType, String content)
    {
        super();
        this.hbType = hbType;
        this.content = content;
    }

    /**
     * @return the hbType
     */
    public String getHbType()
    {
        return hbType;
    }

    /**
     * @param hbType the hbType to set
     */
    public void setHbType(String hbType)
    {
        this.hbType = hbType;
    }

    /**
     * @return the content
     */
    public String getContent()
    {
        return content;
    }

    /**
     * @param content the content to set
     */
    public void setContent(String content)
    {
        this.content = content;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("HeartBeatResp [hbType=").append(hbType)
                .append(", content=").append(content).append("]");
        return builder.toString();
    }
}
