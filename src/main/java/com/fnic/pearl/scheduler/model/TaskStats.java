/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

import com.google.gson.annotations.Expose;

/**
 * 维护所有探针上的任务统计信息
 * 
 * @author HuHaiyang
 * @date 2013年10月12日
 */
public class TaskStats
{
    private static TaskStats instance = new TaskStats();
    public static TaskStats getInstance()
    {
        return instance;
    }
    
    @Expose
    private Map<Integer, ProbeTaskStats> stats = new HashMap<Integer, ProbeTaskStats>();
    private final ReentrantLock lockStats = new ReentrantLock();
    
    // 启动以来接收过的总的任务数
    @Expose
    private long totalRecvTaskNum;
    private final ReentrantLock lockRecvTaskNum = new ReentrantLock();
    
    public ProbeTaskStats getStats(int probeId)
    {
        return stats.get(probeId);
    }
    
    public void incrRecvTaskNum()
    {
        lockRecvTaskNum.lock();
        ++totalRecvTaskNum;
        lockRecvTaskNum.unlock();
    }
    
    public void incrInitTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.incrInitTaskNum();
        lockStats.unlock();
    }
    
    public void decrInitTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.decrInitTaskNum();
        lockStats.unlock();
    }
    
    public void incrWissueTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.incrWissueTaskNum();
        lockStats.unlock();
    }
    
    public void decrWissueTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.decrWissueTaskNum();
        lockStats.unlock();
    }
    
    public void incrIssuedTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.incrIssuedTaskNum();
        lockStats.unlock();
    }
    
    public void decrIssuedTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.decrIssuedTaskNum();
        lockStats.unlock();
    }
    
    public void incrRunningTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.incrRunningTaskNum();
        lockStats.unlock();
    }
    
    public void decrRunningTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.decrRunningTaskNum();
        lockStats.unlock();
    }
    
    public void incrExecSuccTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.incrExecSuccTaskNum();
        lockStats.unlock();
    }
    
    public void decrExecSuccTaskNum(int probeId)
    {
        lockStats.lock();
        if (!stats.containsKey(probeId))
        {
            ProbeTaskStats pts = new ProbeTaskStats();
            stats.put(probeId, pts);
        }
        ProbeTaskStats pts = stats.get(probeId);
        pts.decrExecSuccTaskNum();
        lockStats.unlock();
    }

    /**
     * @return the stats
     */
    public Map<Integer, ProbeTaskStats> getStats()
    {
        return stats;
    }
    
    /**
     * @param stats the stats to set
     */
    public void setStats(Map<Integer, ProbeTaskStats> stats)
    {
        this.stats = stats;
    }
    
    /**
     * @return the totalRecvTaskNum
     */
    public long getTotalRecvTaskNum()
    {
        return totalRecvTaskNum;
    }

    /**
     * @param totalRecvTaskNum the totalRecvTaskNum to set
     */
    public void setTotalRecvTaskNum(long totalRecvTaskNum)
    {
        this.totalRecvTaskNum = totalRecvTaskNum;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("TaskStats [stats=").append(stats).append("]");
        return builder.toString();
    }
}
