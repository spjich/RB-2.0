package com.fnic.pearl.scheduler.model;

import com.fnic.pearl.scheduler.annotations.OldTask;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * 该 annotation 策略用于当 taskType 为 testask 时
 * 不需要 serialize or deserialize [extendInfo]
 */
public class GenericTaskAnnotationExclusionStrategy implements ExclusionStrategy {
	
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getAnnotation(OldTask.class) == null;
	}

	public boolean shouldSkipClass(Class<?> clazz) {
		return clazz.getAnnotation(OldTask.class) == null;
	}
}