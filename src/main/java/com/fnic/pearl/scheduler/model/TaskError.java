/**
 * 
 */
package com.fnic.pearl.scheduler.model;

/**
 * 测量任务错误信息
 * 
 * @author HuHaiyang
 * @date 2014/04/25
 */
public class TaskError
{
	private long taskId;
    private int errorCode;
    
    public TaskError()
    {
        taskId = 0;
        errorCode = 0;
    }
    
    public TaskError(long taskId, int errorCode)
    {
        super();
        this.taskId = taskId;
        this.errorCode = errorCode;
    }
        
    /**
     * @return the taskId
     */
    public long getTaskId()
    {
        return taskId;
    }
    
    /**
     * @param taskId the taskId to set
     */
    public void setTaskId(long taskId)
    {
        this.taskId = taskId;
    }
    
    /**
     * @return the errorCode
     */
    public int getErrorCode()
    {
        return errorCode;
    }
    
    /**
     * @param errorCode the errorCode to set
     */
    public void setErrorCode(int errorCode)
    {
        this.errorCode = errorCode;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("TaskError [taskId=").append(taskId)
                .append(", errorCode=").append(errorCode).append("]");
        return builder.toString();
    }
}
