/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 探针退出请求
 * 
 * @author Steven Hu
 * @date 2014/04/28
 */
public class ProbeLogoutReq
{
	// 探针标识
    @Expose
    @SerializedName("probeId")
	private int probeId;

	/**
	 * @return the probeId
	 */
	public int getProbeId() {
		return probeId;
	}

	/**
	 * @param probeId the probeId to set
	 */
	public void setProbeId(int probeId) {
		this.probeId = probeId;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProbeLogoutReq [probeId=").append(probeId).append("]");
		return builder.toString();
	}
}
