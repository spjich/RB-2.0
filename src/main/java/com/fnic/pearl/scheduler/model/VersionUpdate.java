/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * 版本升级记录
 * 
 * @author HuHaiyang
 * @date 2014年2月14日
 */
public class VersionUpdate
{

    @Expose
    private int updateId;

    @Expose
    private int moduleType;

    @Expose
    private int moduleId;

    // @Expose
    // private int subModuleType;

    @Expose
    private int subModuleId;

    @Expose
    private String pearlVersion;

    @Expose
    private String updateVersion;

    @Expose
    private String pkgUrl;

    @Expose
    private List<String> dependencies;

    @Expose
    private List<String> patches;

    @Expose
    private String execCmd;

    @Expose
    private Integer updateAction;

    /**
     * @return the updateId
     */
    public int getUpdateId()
    {
        return updateId;
    }

    /**
     * @param updateId the updateId to set
     */
    public void setUpdateId(int updateId)
    {
        this.updateId = updateId;
    }

    /**
     * @return the moduleType
     */
    public int getModuleType()
    {
        return moduleType;
    }

    /**
     * @param moduleType the moduleType to set
     */
    public void setModuleType(int moduleType)
    {
        this.moduleType = moduleType;
    }

    /**
     * @return the moduleId
     */
    public int getModuleId()
    {
        return moduleId;
    }

    /**
     * @param moduleId the moduleId to set
     */
    public void setModuleId(int moduleId)
    {
        this.moduleId = moduleId;
    }

    // /**
    // * @return the subModuleType
    // */
    // public int getSubModuleType()
    // {
    // return subModuleType;
    // }
    //
    // /**
    // * @param subModuleType the subModuleType to set
    // */
    // public void setSubModuleType(int subModuleType)
    // {
    // this.subModuleType = subModuleType;
    // }

    /**
     * @return the subModuleId
     */
    public int getSubModuleId()
    {
        return subModuleId;
    }

    /**
     * @param subModuleId the subModuleId to set
     */
    public void setSubModuleId(int subModuleId)
    {
        this.subModuleId = subModuleId;
    }

    /**
     * @return the pearlVersion
     */
    public String getPearlVersion()
    {
        return pearlVersion;
    }

    /**
     * @param pearlVersion the pearlVersion to set
     */
    public void setPearlVersion(String pearlVersion)
    {
        this.pearlVersion = pearlVersion;
    }

    /**
     * @return the updateVersion
     */
    public String getUpdateVersion()
    {
        return updateVersion;
    }

    /**
     * @param updateVersion the updateVersion to set
     */
    public void setUpdateVersion(String updateVersion)
    {
        this.updateVersion = updateVersion;
    }

    /**
     * @return the pkgUrl
     */
    public String getPkgUrl()
    {
        return pkgUrl;
    }

    /**
     * @param pkgUrl the pkgUrl to set
     */
    public void setPkgUrl(String pkgUrl)
    {
        this.pkgUrl = pkgUrl;
    }

    /**
     * @return the dependencies
     */
    public List<String> getDependencies()
    {
        return dependencies;
    }

    /**
     * @param dependencies the dependencies to set
     */
    public void setDependencies(List<String> dependencies)
    {
        this.dependencies = dependencies;
    }

    /**
     * @return the patches
     */
    public List<String> getPatches()
    {
        return patches;
    }

    /**
     * @param patches the patches to set
     */
    public void setPatches(List<String> patches)
    {
        this.patches = patches;
    }

    /**
     * @return the execCmd
     */
    public String getExecCmd()
    {
        return execCmd;
    }

    /**
     * @param execCmd the execCmd to set
     */
    public void setExecCmd(String execCmd)
    {
        this.execCmd = execCmd;
    }

    public Integer getUpdateAction()
    {
        return updateAction;
    }

    public void setUpdateAction(Integer updateAction)
    {
        this.updateAction = updateAction;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("VersionUpdate [updateId=").append(updateId).append(", moduleType=").append(moduleType)
                .append(", moduleId=").append(moduleId).append(", subModuleId=").append(subModuleId)
                .append(", pearlVersion=").append(pearlVersion).append(", updateVersion=").append(updateVersion)
                .append(", pkgUrl=").append(pkgUrl).append(", dependencies=").append(dependencies).append(", patches=")
                .append(patches).append(", execCmd=").append(execCmd).append("]");
        return builder.toString();
    }
}
