/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import com.google.gson.annotations.Expose;

/**
 * 升级状态
 * 
 * @author HuHaiyang
 * @date 2014年2月17日
 */
public class VersionUpdateStatus
{
    @Expose
    private int updateId;
    
    @Expose
    private int moduleType;
    
    @Expose
    private int moduleId;
    
    @Expose
    private String currentVersion;
    
    @Expose
    private String updateVersion;
    
    @Expose
    private int updateState;
    
    @Expose
    private String remark;

    @Expose
    private Integer subModuleType;

    /**
     * @return the updateId
     */
    public int getUpdateId()
    {
        return updateId;
    }

    /**
     * @param updateId the updateId to set
     */
    public void setUpdateId(int updateId)
    {
        this.updateId = updateId;
    }

    /**
     * @return the moduleType
     */
    public int getModuleType()
    {
        return moduleType;
    }

    /**
     * @param moduleType the moduleType to set
     */
    public void setModuleType(int moduleType)
    {
        this.moduleType = moduleType;
    }

    /**
     * @return the moduleId
     */
    public int getModuleId()
    {
        return moduleId;
    }

    /**
     * @param moduleId the moduleId to set
     */
    public void setModuleId(int moduleId)
    {
        this.moduleId = moduleId;
    }

    /**
     * @return the currentVersion
     */
    public String getCurrentVersion()
    {
        return currentVersion;
    }

    /**
     * @param currentVersion the currentVersion to set
     */
    public void setCurrentVersion(String currentVersion)
    {
        this.currentVersion = currentVersion;
    }

    /**
     * @return the updateVersion
     */
    public String getUpdateVersion()
    {
        return updateVersion;
    }

    /**
     * @param updateVersion the updateVersion to set
     */
    public void setUpdateVersion(String updateVersion)
    {
        this.updateVersion = updateVersion;
    }

    /**
     * @return the state
     */
    public int getUpdateState()
    {
        return updateState;
    }

    /**
     * @param state the state to set
     */
    public void setUpdateState(int updateState)
    {
        this.updateState = updateState;
    }

    /**
     * @return the remark
     */
    public String getRemark()
    {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark)
    {
        this.remark = remark;
    }

    public Integer getSubModuleType()
    {
        return subModuleType;
    }

    public void setSubModuleType(Integer subModuleType)
    {
        this.subModuleType = subModuleType;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("UpdateStatus [updateId=").append(updateId)
                .append(", moduleType=").append(moduleType)
                .append(", moduleId=").append(moduleId)
                .append(", currentVersion=").append(currentVersion)
                .append(", updateVersion=").append(updateVersion)
                .append(", state=").append(updateState).append(", remark=")
                .append(remark).append("]");
        return builder.toString();
    }
}
