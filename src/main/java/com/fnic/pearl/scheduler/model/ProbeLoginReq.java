/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 探针登录请求
 * 
 * @author Steven Hu
 * @date 2014/04/28
 */
public class ProbeLoginReq
{

    // 探针标识
    @Expose
    @SerializedName("probeId")
    private int probeId;

    // begin add by yyzhang 20140925 support show probeName
    // 探针名称
    @Expose
    @SerializedName("probeName")
    private String probeName;

    // end add by yyzhang 20140925 support show probeName

    // 探针标识
    @Expose
    @SerializedName("probeVer")
    private String probeVer;

    // mac 地址列表
    @Expose
    @SerializedName("macList")
    private List<String> macList;

    // mac 地址列表
    @Expose
    @SerializedName("icareType")
    private int icareType;

    // mac 地址列表
    @Expose
    @SerializedName("serial")
    private String serial;

    /**
     * @return the probeId
     */
    public int getProbeId()
    {
        return probeId;
    }

    /**
     * @param probeId the probeId to set
     */
    public void setProbeId(int probeId)
    {
        this.probeId = probeId;
    }

    /**
     * 登陆请求中获取探针名称
     * 
     * @return
     */
    public String getProbeName()
    {
        return probeName;
    }

    /**
     * 设置探针名称
     * 
     * @return
     */
    public void setProbeName(String probeName)
    {
        this.probeName = probeName;
    }

    /**
     * @return the probeVer
     */
    public String getProbeVer()
    {
        return probeVer;
    }

    /**
     * @param probeVer the probeVer to set
     */
    public void setProbeVer(String probeVer)
    {
        this.probeVer = probeVer;
    }

    /**
     * @return the macList
     */
    public List<String> getMacList()
    {
        return macList;
    }

    /**
     * @param macList the macList to set
     */
    public void setMacList(List<String> macList)
    {
        this.macList = macList;
    }

    /**
     * @return the icareType
     */
    public int getIcareType()
    {
        return icareType;
    }

    /**
     * @param icareType the icareType to set
     */
    public void setIcareType(int icareType)
    {
        this.icareType = icareType;
    }

    /**
     * @return the serial
     */
    public String getSerial()
    {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial)
    {
        this.serial = serial;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("ProbeLoginReq [probeId=").append(probeId).append(", probeVer=").append(probeVer)
                .append(", macList=").append(macList).append(", icareType=").append(icareType).append(", serial=")
                .append(serial).append("]");
        return builder.toString();
    }
}
