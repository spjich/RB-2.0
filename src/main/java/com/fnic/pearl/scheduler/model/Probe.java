/**
 * 
 */
package com.fnic.pearl.scheduler.model;

/**
 * 探针信息
 * 
 * @author HuHaiyang
 * @date 2013年7月14日
 */
public class Probe
{
    // 探针标识
    private int probeId;
    
    // 探针 serial
    private String serial;
    
    // 设备类型
    private int deviceType;
    
    // 软件版本
    private String version;
    
    // 设备硬件级别
    private int deviceGrade;
    
    // 内核版本号
    private String kernel;
    
    // shell 版本
    private String shell;
    
    // 加密 key
    private String secretKey;

    /**
     * @return the probeId
     */
    public int getProbeId()
    {
        return probeId;
    }

    /**
     * @param probeId the probeId to set
     */
    public void setProbeId(int probeId)
    {
        this.probeId = probeId;
    }

    /**
     * @return the serial
     */
    public String getSerial()
    {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial)
    {
        this.serial = serial;
    }

    /**
     * @return the deviceType
     */
    public int getDeviceType()
    {
        return deviceType;
    }

    /**
     * @param deviceType the deviceType to set
     */
    public void setDeviceType(int deviceType)
    {
        this.deviceType = deviceType;
    }

    /**
     * @return the version
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version)
    {
        this.version = version;
    }

    /**
     * @return the deviceGrade
     */
    public int getDeviceGrade()
    {
        return deviceGrade;
    }

    /**
     * @param deviceGrade the deviceGrade to set
     */
    public void setDeviceGrade(int deviceGrade)
    {
        this.deviceGrade = deviceGrade;
    }

    /**
     * @return the kernel
     */
    public String getKernel()
    {
        return kernel;
    }

    /**
     * @param kernel the kernel to set
     */
    public void setKernel(String kernel)
    {
        this.kernel = kernel;
    }

    /**
     * @return the shell
     */
    public String getShell()
    {
        return shell;
    }

    /**
     * @param shell the shell to set
     */
    public void setShell(String shell)
    {
        this.shell = shell;
    }

    /**
     * @return the secretKey
     */
    public String getSecretKey()
    {
        return secretKey;
    }

    /**
     * @param secretKey the secretKey to set
     */
    public void setSecretKey(String secretKey)
    {
        this.secretKey = secretKey;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("Probe [probeId=").append(probeId).append(", serial=")
                .append(serial).append(", deviceType=").append(deviceType)
                .append(", version=").append(version).append(", deviceGrade=")
                .append(deviceGrade).append(", kernel=").append(kernel)
                .append(", shell=").append(shell).append(", secretKey=")
                .append(secretKey).append("]");
        return builder.toString();
    }
}
