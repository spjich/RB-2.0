/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import java.util.concurrent.locks.ReentrantLock;

import com.google.gson.annotations.Expose;

/**
 * 某一探针上的任务统计信息
 * 
 * @author HuHaiyang
 * @date 2013年10月11日
 */
public class ProbeTaskStats
{
    @Expose
    private long initTaskNum;
    private ReentrantLock lockInit = new ReentrantLock();
    
    @Expose
    private long wissueTaskNum;
    private ReentrantLock lockWissue = new ReentrantLock();
    
    @Expose
    private long issuedTaskNum;
    private ReentrantLock lockIssued = new ReentrantLock();
    
    @Expose
    private long runningTaskNum;
    private ReentrantLock lockRunning = new ReentrantLock();
    
    private long execSuccTaskNum;
    private ReentrantLock lockExecSucc = new ReentrantLock();
    
    public void incrInitTaskNum()
    {
        lockInit.lock();
        ++initTaskNum;
        lockInit.unlock();
    }
    
    public void decrInitTaskNum()
    {
        lockInit.lock();
        --initTaskNum;
        lockInit.unlock();
    }
    
    public void incrWissueTaskNum()
    {
        lockWissue.lock();
        ++wissueTaskNum;
        lockWissue.unlock();
    }
    
    public void decrWissueTaskNum()
    {
        lockWissue.lock();
        --wissueTaskNum;
        lockWissue.unlock();
    }
    
    public void incrIssuedTaskNum()
    {
        lockIssued.lock();
        ++issuedTaskNum;
        lockIssued.unlock();
    }
    
    public void decrIssuedTaskNum()
    {
        lockIssued.lock();
        --issuedTaskNum;
        lockIssued.unlock();
    }
    
    public void incrRunningTaskNum()
    {
        lockRunning.lock();
        ++runningTaskNum;
        lockRunning.unlock();
    }
    
    public void decrRunningTaskNum()
    {
        lockRunning.lock();
        --runningTaskNum;
        lockRunning.unlock();
    }
    
    public void incrExecSuccTaskNum()
    {
        lockExecSucc.lock();
        ++execSuccTaskNum;
        lockExecSucc.unlock();
    }
    
    public void decrExecSuccTaskNum()
    {
        lockExecSucc.lock();
        --execSuccTaskNum;
        lockExecSucc.unlock();
    }
    
    /**
     * @return the initTaskNum
     */
    public long getInitTaskNum()
    {
        return initTaskNum;
    }

    /**
     * @return the wissueTaskNum
     */
    public long getWissueTaskNum()
    {
        return wissueTaskNum;
    }
    
    /**
     * @return the issuedTaskNum
     */
    public long getIssuedTaskNum()
    {
        return issuedTaskNum;
    }

    /**
     * @return the runningTaskNum
     */
    public long getRunningTaskNum()
    {
        return runningTaskNum;
    }

    /**
     * @return the execSuccTaskNum
     */
    public long getExecSuccTaskNum()
    {
        return execSuccTaskNum;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("TaskStats [initTaskNum=").append(initTaskNum)
                .append(", lockInit=").append(lockInit)
                .append(", wissueTaskNum=").append(wissueTaskNum)
                .append(", lockWissue=").append(lockWissue)
                .append(", issuedTaskNum=").append(issuedTaskNum).append("]");
        return builder.toString();
    }
}
