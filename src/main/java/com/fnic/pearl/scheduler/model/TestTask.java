/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fnic.pearl.scheduler.annotations.OldTask;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * 测试任务记录
 * 
 * @author HuHaiyang
 * @date 2013/07/14
 */
public class TestTask
{
    // 测量任务运行类型
    public static final int RUN_IMMEDIATE = 1;
    public static final int RUN_TIMING = 2;
    
	// 任务标识
    @Expose(serialize=true, deserialize=true)
    @SerializedName("taskId")
	private long taskId;
    
    // 任务类型
    @Expose(serialize=true, deserialize=true)
    @SerializedName("taskType")
    private String taskType;
    
    // 当前已执行的次数
    @Expose(serialize=true, deserialize=true)
    @SerializedName("execNum")
    private int execNum = 0;
	
	// 启动方式
    @Expose(serialize=true, deserialize=true)
    @SerializedName("run")
	private int run;
	
	// 测试任务开始时间
    @Expose(deserialize=true)
    @SerializedName("startTime")
    @JsonDeserialize(using = CustomDateDeserializer.class)
	private Date startTime;
	
	// 测试任务结束时间
    @Expose(deserialize=true)
    @SerializedName("endTime")
    @JsonDeserialize(using = CustomDateDeserializer.class)
	private Date endTime;
	
	// 测试次数
    @Expose(deserialize=true)
    @SerializedName("count")
	private int count;
	
	// 测试周期
    @Expose(deserialize=true)
    @SerializedName("periodSec")
	private int periodSec;
    
    // 需要执行该任务的探针节点 ID
    @Expose(deserialize=true)
    @SerializedName("probe")
	private Integer probe;
    
    /*---------------*/
    
    // 工具标识
    @OldTask
    @SerializedName("toolId")
	private int toolId;
    
    @OldTask
    @Expose(serialize=true, deserialize=true)
    @SerializedName("tool")
    private String toolName;
	
	// 输入模板标识
    @OldTask
    @Expose(serialize=true, deserialize=true)
    @SerializedName("inTempId")
	private int inTempId;
	
    // 输入模板参数列表
    @OldTask
    @Expose(serialize=true, deserialize=true)
    @SerializedName("inTempParams")
    private List<String> inTempParams;
	
    // 需要执行该任务的探针节点的 serial
    @OldTask
    @Expose(deserialize=true)
    @SerializedName("serial")
    private String serial;
    
    // 是否需要实时执行
    @OldTask
    @Expose(serialize=true, deserialize=true)
    @SerializedName("realTime")
    private int realtime;
    
    // 当前状态
    @Expose(serialize=true, deserialize=false)
    private int status;
    
    // 对于当前状态处于 hangup 状态的任务
    // 还需要保存 hangup 前的状态
    private int lastStatus;
    
    // 上一次状态切换的时间，取 currentMillSeconds
    private long lastStatusTime;
    
    // 上一次任务执行结束的时间
    @Expose(serialize=true, deserialize=false)
    private Date lastEndTime;
    
    // 任务的扩展属性
    @Expose(serialize=true, deserialize=true)
    @SerializedName("extendInfo")
    private Object extendInfo;

	/**
	 * @return the taskId
	 */
	public long getTaskId()
	{
		return taskId;
	}

	/**
	 * @param taskId the taskId to set
	 */
	public void setTaskId(long taskId)
	{
		this.taskId = taskId;
	}

	/**
	 * @return the taskType
	 */
	public String getTaskType() {
		return taskType;
	}

	/**
	 * @param taskType the taskType to set
	 */
	public void setTaskType(String taskType) {
		this.taskType = taskType;
	}

	/**
     * @return the execNum
     */
    public int getExecNum()
    {
        return execNum;
    }

    /**
     * @param execNum the execNum to set
     */
    public void setExecNum(int execNum)
    {
        this.execNum = execNum;
    }

	/**
	 * @return the toolId
	 */
	public int getToolId()
	{
		return toolId;
	}

	/**
	 * @param toolId the toolId to set
	 */
	public void setToolId(int toolId)
	{
		this.toolId = toolId;
	}

	/**
     * @return the toolName
     */
    public String getToolName()
    {
        return toolName;
    }

    /**
     * @param toolName the toolName to set
     */
    public void setToolName(String toolName)
    {
        this.toolName = toolName;
    }

    /**
	 * @return the runType
	 */
	public int getRun()
	{
		return run;
	}
	
	/**
	 * @param run the runType to set
	 */
	public void setRun(int run)
	{
		this.run = run;
	}

	/**
	 * @return the startTime
	 */
	public Date getStartTime()
	{
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(Date startTime)
	{
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public Date getEndTime()
	{
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(Date endTime)
	{
		this.endTime = endTime;
	}

	/**
	 * @return the count
	 */
	public int getCount()
	{
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count)
	{
		this.count = count;
	}

	/**
	 * @return the periodSec
	 */
	public int getPeriodSec()
	{
		return periodSec;
	}

	/**
	 * @param periodSec the periodSec to set
	 */
	public void setPeriodSec(int periodSec)
	{
		this.periodSec = periodSec;
	}

	/**
	 * @return the inTempId
	 */
	public int getInTempId()
	{
		return inTempId;
	}

	/**
	 * @param inTempId the inTempId to set
	 */
	public void setInTempId(int inTempId)
	{
		this.inTempId = inTempId;
	}

    /**
     * @return the inTempParams
     */
    public List<String> getInTempParams()
    {
        return inTempParams;
    }

    /**
     * @param inTempParams the inTempParams to set
     */
    public void setInTempParams(List<String> inTempParams)
    {
        this.inTempParams = inTempParams;
    }

    /**
     * @return the probe
     */
    public Integer getProbe()
    {
        return probe;
    }

    /**
     * @param probe the probe to set
     */
    public void setProbe(Integer probe)
    {
        this.probe = probe;
    }

    /**
     * @return the serial
     */
    public String getSerial()
    {
        return serial;
    }

    /**
     * @param serial the serial to set
     */
    public void setSerial(String serial)
    {
        this.serial = serial;
    }

    /**
     * @return the realtime
     */
    public int getRealtime()
    {
        return realtime;
    }

    /**
     * @param realtime the realtime to set
     */
    public void setRealtime(int realtime)
    {
        this.realtime = realtime;
    }

    /**
     * @return the status
     */
    public int getStatus()
    {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status)
    {
        this.status = status;
    }
    
    /**
     * @return the lastStatus
     */
    public int getLastStatus()
    {
        return lastStatus;
    }

    /**
     * @param lastStatus the lastStatus to set
     */
    public void setLastStatus(int lastStatus)
    {
        this.lastStatus = lastStatus;
    }

    /**
     * @return the lastStatusTime
     */
    public long getLastStatusTime()
    {
        return lastStatusTime;
    }

    /**
     * @param lastStatusTime the lastStatusTime to set
     */
    public void setLastStatusTime(long lastStatusTime)
    {
        this.lastStatusTime = lastStatusTime;
    }

    /**
     * @return the lastEndTime
     */
    public Date getLastEndTime()
    {
        return lastEndTime;
    }

    /**
     * @param lastEndTime the lastEndTime to set
     */
    public void setLastEndTime(Date lastEndTime)
    {
        this.lastEndTime = lastEndTime;
    }

    /**
	 * @return the extendInfo
	 */
	public Object getExtendInfo() {
		return extendInfo;
	}

	/**
	 * @param extendInfo the extendInfo to set
	 */
	public void setExtendInfo(Object extendInfo) {
		this.extendInfo = extendInfo;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TestTask [taskId=" + taskId + ", taskType=" + taskType
				+ ", execNum=" + execNum + ", toolId=" + toolId + ", toolName="
				+ toolName + ", runType=" + run + ", startTime="
				+ startTime + ", endTime=" + endTime + ", count=" + count
				+ ", periodSec=" + periodSec + ", inTempId=" + inTempId
				+ ", inTempParams=" + inTempParams + ", probe=" + probe
				+ ", serial=" + serial + ", realtime=" + realtime + ", status="
				+ status + ", lastStatus=" + lastStatus + ", lastStatusTime="
				+ lastStatusTime + ", lastEndTime=" + lastEndTime
				+ ", extendInfo=" + extendInfo + "]";
	}
}
