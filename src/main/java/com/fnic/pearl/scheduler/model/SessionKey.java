/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * session key
 * 
 * @author HuHaiyang
 * @date 2013年11月14日
 */
public class SessionKey {
	@Expose
	@SerializedName("sessionKey")
	private String sessionKey;

	@Expose(deserialize = true, serialize = false)
	private int probeId;

	@Expose(deserialize = true, serialize = false)
	private int timeout;

	// 获取 session key 的时间
	private Date createTime;

	// 上一个 session key
	private String prevSessionKey;

	// 上一个 session key 的创建时间
	private Date prevCreateTime;

	public SessionKey(int probeId) {
		this.probeId = probeId;
		this.timeout = 1000 * 600;
		this.sessionKey = "a9aa1c61-2306-4d6e-a0b0-2dab369bf7b8";
	}

	/**
	 * @return the createTime
	 */
	public Date getCreateTime() {
		return createTime;
	}

	/**
	 * @param createTime
	 *            the createTime to set
	 */
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
	 * @return the probeId
	 */
	public int getProbeId() {
		return probeId;
	}

	/**
	 * @param probeId
	 *            the probeId to set
	 */
	public void setProbeId(int probeId) {
		this.probeId = probeId;
	}

	/**
	 * @return the timeout
	 */
	public int getTimeout() {
		return timeout;
	}

	/**
	 * @param timeout
	 *            the timeout to set
	 */
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	/**
	 * @return the prevSessionKey
	 */
	public String getPrevSessionKey() {
		return prevSessionKey;
	}

	/**
	 * @param prevSessionKey
	 *            the prevSessionKey to set
	 */
	public void setPrevSessionKey(String prevSessionKey) {
		this.prevSessionKey = prevSessionKey;
	}

	/**
	 * @return the prevCreateTime
	 */
	public Date getPrevCreateTime() {
		return prevCreateTime;
	}

	/**
	 * @param prevCreateTime
	 *            the prevCreateTime to set
	 */
	public void setPrevCreateTime(Date prevCreateTime) {
		this.prevCreateTime = prevCreateTime;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SessionKey [sessionKey=").append(sessionKey).append(", probeId=").append(probeId)
				.append(", timeout=").append(timeout).append(", createTime=").append(createTime)
				.append(", prevSessionKey=").append(prevSessionKey).append(", prevCreateTime=").append(prevCreateTime)
				.append("]");
		return builder.toString();
	}

	/**
	 * @return the sessionKey
	 */
	public String getSessionKey() {
		return sessionKey;
	}

	/**
	 * @param sessionKey
	 *            the sessionKey to set
	 */
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
}
