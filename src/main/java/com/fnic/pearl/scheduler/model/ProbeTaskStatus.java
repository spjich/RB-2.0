/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import java.util.Calendar;
import java.util.Date;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * 指定探针下的任务及执行状态信息
 * 
 * @author HuHaiyang
 * @date 2013年7月14日
 */
public class ProbeTaskStatus
{
    // 探针标识
    @Expose(serialize=false, deserialize=true)
    @SerializedName("probeId")
    private int probeId;
    
    // 任务标识
    @SerializedName("taskId")
    @Expose
    private long taskId;
    
    // 测试任务当前状态
    @SerializedName("status")
    @Expose
    private int status;
    
    // 任务执行的次数
    @SerializedName("execNum")
    @Expose
    private int execNum;
    
    // 状态时间，date 类型
    @Expose
    @SerializedName("statusTime")
    private Date dateStatusTime;
    
    // 错误码
    @Expose
    @SerializedName("errorNo")
    private int errorNo;
    
    // 错误消息
    @Expose
    @SerializedName("errorMsg")
    private String errorMsg;
    
    public static final int S_FINAL_NOT = 0;
    public static final int S_FINAL_OVER = 1;
    
    // 任务终态信息
    @Expose(serialize=true, deserialize=false)
    @SerializedName("final")
    private int finalStatus;
    
    public ProbeTaskStatus()
    {
        this.probeId = 0;
        this.taskId = 0;
        this.status = 0;
        this.errorNo = 0;
        this.errorMsg = null;
        this.finalStatus = S_FINAL_NOT;
        this.dateStatusTime = Calendar.getInstance().getTime();
    }
    
    public ProbeTaskStatus(int probeId, long taskId, int status)
    {
        super();
        this.probeId = probeId;
        this.taskId = taskId;
        this.status = status;
        this.errorNo = 0;
        this.errorMsg = null;
        this.finalStatus = S_FINAL_NOT;
        this.dateStatusTime = new Date(); 
    }
    
    /**
     * @return the errorNo
     */
    public int getErrorNo()
    {
        return errorNo;
    }

    /**
     * @param errorNo the errorNo to set
     */
    public void setErrorNo(int errorNo)
    {
        this.errorNo = errorNo;
    }

    /**
     * @return the errorMsg
     */
    public String getErrorMsg()
    {
        return errorMsg;
    }

    /**
     * @param errorMsg the errorMsg to set
     */
    public void setErrorMsg(String errorMsg)
    {
        this.errorMsg = errorMsg;
    }

    /**
     * @return the probeId
     */
    public int getProbeId()
    {
        return probeId;
    }

    /**
     * @param probeId the probeId to set
     */
    public void setProbeId(int probeId)
    {
        this.probeId = probeId;
    }

    /**
     * @return the taskId
     */
    public long getTaskId()
    {
        return taskId;
    }

    /**
     * @param taskId the taskId to set
     */
    public void setTaskId(long taskId)
    {
        this.taskId = taskId;
    }

    /**
     * @return the status
     */
    public int getStatus()
    {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(int status)
    {
        this.status = status;
    }
    
    /**
     * @return the execNum
     */
    public int getExecNum()
    {
        return execNum;
    }

    /**
     * @param execNum the execNum to set
     */
    public void setExecNum(int execNum)
    {
        this.execNum = execNum;
    }

    /**
     * @return the dateStatusTime
     */
    public Date getDateStatusTime()
    {
        return dateStatusTime;
    }

    /**
     * @param dateStatusTime the dateStatusTime to set
     */
    public void setDateStatusTime(Date dateStatusTime)
    {
        this.dateStatusTime = dateStatusTime;
    }

    /**
     * @return the finalStatus
     */
    public int getFinalStatus()
    {
        return finalStatus;
    }

    /**
     * @param finalStatus the finalStatus to set
     */
    public void setFinalStatus(int finalStatus)
    {
        this.finalStatus = finalStatus;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("ProbeTaskStatus [probeId=").append(probeId)
                .append(", taskId=").append(taskId).append(", status=")
                .append(status).append(", execNum=").append(execNum)
                .append(", dateStatusTime=").append(dateStatusTime)
                .append(", errorNo=").append(errorNo).append(", errorMsg=")
                .append(errorMsg).append(", finalStatus=").append(finalStatus)
                .append("]");
        return builder.toString();
    }
}
