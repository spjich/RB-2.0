/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import java.util.Date;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 探针状态，只是记录最近一次的登录和退出时间
 * 
 * @author HuHaiyang
 * @date 2013年8月20日
 */
public class ProbeStatus {
	private ReentrantLock __lock = new ReentrantLock(true);

	public static final int S_CONNECTED = 1;

	public static final int S_LOGIN = 2;

	public static final int S_LOGOUT = 3;

	public static final int S_DISCONNECT = 4;

	// 探针标识
	private int probeId;

	// begin add by yyzhang 20140925 support show probeName
	// 探针名称
	private String probeName;

	// end add by yyzhang 20140925 support show probeName

	// 探针版本
	private String probeVer = "0.0.0";

	// 使用的协议版本
	private String loginProtoVer;

	// 当前状态
	private int status;

	// 最后一次登录的地址
	private String lastLoginIp;

	// 最后一次登录的端口
	private int lastLoginPort;

	// 最后一次登录时间
	private Date lastLoginTime;

	// 最后一次登出时间
	private Date lastLogoutTime;

	// 当前是否正在处理心跳
	private boolean isHandlingHB;

	// 最后一次接收心跳的时间
	private Date lastRecvHBTime;

	// 最后一次处理完心跳的时间
	private Date lastHandleHBTime;

	// mac 地址
	private String mac;

	// 地理位置 以及 ISP 提供商
	private AddressInfo address;

	// 探针类型
	private int icareType;

	// 探针 serial
	private String probeSerial;

	// Session key
	private SessionKey sessionKey;

	// serial
	private String serial;

	public ProbeStatus() {
		this.isHandlingHB = false;
		this.status = S_LOGOUT;
		this.mac = null;
	}

	/**
	 * @return the probeId
	 */
	public int getProbeId() {
		return probeId;
	}

	/***
	 * 获取探针名称
	 * 
	 * @return
	 */
	public String getProbeName() {
		return probeName;
	}

	/***
	 * 设置探针名称
	 * 
	 * @return
	 */
	public void setProbeName(String probeName) {
		this.probeName = probeName;
	}

	/**
	 * @param probeId
	 *            the probeId to set
	 */
	public void setProbeId(int probeId) {
		this.probeId = probeId;
	}

	/**
	 * @return the probeVer
	 */
	public String getProbeVer() {
		return probeVer;
	}

	/**
	 * @param probeVer
	 *            the probeVer to set
	 */
	public void setProbeVer(String probeVer) {
		this.probeVer = probeVer;
	}

	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * @return the lastLogin
	 */
	public Date getLastLoginTime() {
		return lastLoginTime;
	}

	/**
	 * @param lastLogin
	 *            the lastLogin to set
	 */
	public void setLastLoginTime(Date lastLogin) {
		this.lastLoginTime = lastLogin;
	}

	/**
	 * @return the lastLogout
	 */
	public Date getLastLogoutTime() {
		return lastLogoutTime;
	}

	/**
	 * @return the lastLoginIp
	 */
	public String getLastLoginIp() {
		return lastLoginIp;
	}

	/**
	 * @param lastLoginIp
	 *            the lastLoginIp to set
	 */
	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	/**
	 * @return the lastLoginPort
	 */
	public int getLastLoginPort() {
		return lastLoginPort;
	}

	/**
	 * @param lastLoginPort
	 *            the lastLoginPort to set
	 */
	public void setLastLoginPort(int lastLoginPort) {
		this.lastLoginPort = lastLoginPort;
	}

	/**
	 * @param lastLogoutTime
	 *            the lastLogoutTime to set
	 */
	public void setLastLogoutTime(Date lastLogoutTime) {
		this.lastLogoutTime = lastLogoutTime;
	}

	/**
	 * @return the isHandlingHB
	 */
	public boolean isHandlingHB() {
		return isHandlingHB;
	}

	/**
	 * @param isHandlingHB
	 *            the isHandlingHB to set
	 */
	public void setHandlingHB(boolean isHandlingHB) {
		this.isHandlingHB = isHandlingHB;
	}

	/**
	 * @return the lastRecvHBTime
	 */
	public Date getLastRecvHBTime() {
		return lastRecvHBTime;
	}

	/**
	 * @param lastRecvHBTime
	 *            the lastRecvHBTime to set
	 */
	public void setLastRecvHBTime(Date lastRecvHBTime) {
		this.lastRecvHBTime = lastRecvHBTime;
	}

	/**
	 * @return the loginProtoVer
	 */
	public String getLoginProtoVer() {
		return loginProtoVer;
	}

	/**
	 * @param loginProtoVer
	 *            the loginProtoVer to set
	 */
	public void setLoginProtoVer(String loginProtoVer) {
		this.loginProtoVer = loginProtoVer;
	}

	/**
	 * @return the lastRecvHBTime
	 */
	public Date getLastHandleHBTime() {
		return lastHandleHBTime;
	}

	/**
	 * @param lastHandleHBTime
	 *            the lastRecvHBTime to set
	 */
	public void setLastHandleHBTime(Date lastHandleHBTime) {
		this.lastHandleHBTime = lastHandleHBTime;
	}

	public AddressInfo getAddress() {
		return address;
	}

	public void setAddress(AddressInfo address) {
		this.address = address;
	}

	/**
	 * @return the mac
	 */
	public String getMac() {
		return mac;
	}

	/**
	 * @param mac
	 *            the mac to set
	 */
	public void setMac(String mac) {
		this.mac = mac;
	}

	/**
	 * @return the icareType
	 */
	public int getIcareType() {
		return icareType;
	}

	/**
	 * @param icareType
	 *            the icareType to set
	 */
	public void setIcareType(int icareType) {
		this.icareType = icareType;
	}

	/**
	 * @return the probeSerial
	 */
	public String getProbeSerial() {
		return probeSerial;
	}

	/**
	 * @param probeSerial
	 *            the probeSerial to set
	 */
	public void setProbeSerial(String probeSerial) {
		this.probeSerial = probeSerial;
	}

	/**
	 * @return the sessionKey
	 */
	public SessionKey getSessionKey() {
		return sessionKey;
	}

	/**
	 * @param sessionKey
	 *            the sessionKey to set
	 */
	public void setSessionKey(SessionKey sessionKey) {
		this.sessionKey = sessionKey;
	}

	/**
	 * @return the serial
	 */
	public String getSerial() {
		return serial;
	}

	/**
	 * @param serial
	 *            the serial to set
	 */
	public void setSerial(String serial) {
		this.serial = serial;
	}

	public ReentrantLock get__lock() {
		return __lock;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProbeStatus [probeId=").append(probeId).append(", probeVer=").append(probeVer)
				.append(", loginProtoVer=").append(loginProtoVer).append(", status=").append(status)
				.append(", lastLoginIp=").append(lastLoginIp).append(", lastLoginPort=").append(lastLoginPort)
				.append(", lastLoginTime=").append(lastLoginTime).append(", lastLogoutTime=").append(lastLogoutTime)
				.append(", isHandlingHB=").append(isHandlingHB).append(", lastRecvHBTime=").append(lastRecvHBTime)
				.append(", lastHandleHBTime=").append(lastHandleHBTime).append(", mac=").append(mac)
				.append(", icareType=").append(icareType).append(", probeSerial=").append(probeSerial)
				.append(", sessionKey=").append(sessionKey).append(", serial=").append(serial).append("]");
		return builder.toString();
	}
}
