/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import com.google.gson.annotations.Expose;

/**
 * 地理位置信息
 * 
 * @author Steven Hu
 * @date 2013/12/11
 */
public class AddressInfo
{
	@Expose
	private String country;
	
	@Expose
    private String province;
    
	@Expose
	private String city;
    
	@Expose
	private String distinct;
    
	@Expose
	private String isp;
    
	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getProvince() {
		return province;
	}
	
	public void setProvince(String province) {
		this.province = province;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getDistinct() {
		return distinct;
	}
	
	public void setDistinct(String distinct) {
		this.distinct = distinct;
	}
	
	public String getIsp() {
		return isp;
	}
	
	public void setIsp(String isp) {
		this.isp = isp;
	}
	
	@Override
	public String toString() {
		return "AddressInfo [country=" + country + ", province=" + province
				+ ", city=" + city + ", distinct=" + distinct + ", isp=" + isp
				+ "]";
	}
}
