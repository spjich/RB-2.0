/**
 * 
 */
package com.fnic.pearl.scheduler.model;

/**
 * 调度器的版本信息
 * 
 * @author HuHaiyang
 * @date 2013年11月21日
 */
public class SchedulerVersion {

	// 版本号
	public static String version = "V2.0.0.6/Build-1";

	// 编译日期
	public static String compiled = "2016/08/29";

	/**
	 * @return the version
	 */
	public static String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *            the version to set
	 */
	public static void setVersion(String version) {
		SchedulerVersion.version = version;
	}

	/**
	 * @return the compiled
	 */
	public static String getCompiled() {
		return compiled;
	}

	/**
	 * @param compiled
	 *            the compiled to set
	 */
	public static void setCompiled(String compiled) {
		SchedulerVersion.compiled = compiled;
	}
}
