package com.fnic.pearl.scheduler.model;

import com.fnic.pearl.scheduler.SchedulerConfig;

/**
 * 
 * @Title:批量login bean
 * @Description:
 * @Author:吉
 * @Since:2016年5月11日
 * @Version:1.1.0
 */
public class BatchLogin {
	private String probeId;

	private String schedulerId;

	public BatchLogin(ProbeStatus ps) {
		this.probeId = String.valueOf(ps.getProbeId());
		this.schedulerId = String.valueOf(SchedulerConfig.getInstance().getSchedulerId());
	}

	public String getProbeId() {
		return probeId;
	}

	public void setProbeId(String probeId) {
		this.probeId = probeId;
	}

	public String getSchedulerId() {
		return schedulerId;
	}

	public void setSchedulerId(String schedulerId) {
		this.schedulerId = schedulerId;
	}

}
