/**
 * 
 */
package com.fnic.pearl.scheduler.model;

import com.google.gson.annotations.Expose;

/**
 * 升级 probe 的信息
 * 
 * @author HuHaiyang
 * @date 2013年10月18日
 */
public class ProbeUpdate
{
    // 探针标识
    private int probeId;
    
    @Expose
    private int specRpm;
    
    @Expose
    private String pearlVer;
    
    // 版本号
    @Expose
    private String version;
    
    // release 次数
    @Expose
    private int release;
    
    // 升级的安装包 URL
    @Expose
    private String rpmUrl;
    
    /**
     * @return the probeId
     */
    public int getProbeId()
    {
        return probeId;
    }

    /**
     * @param probeId the probeId to set
     */
    public void setProbeId(int probeId)
    {
        this.probeId = probeId;
    }

    /**
     * @return the specRpm
     */
    public int getSpecRpm()
    {
        return specRpm;
    }

    /**
     * @param specRpm the specRpm to set
     */
    public void setSpecRpm(int specRpm)
    {
        this.specRpm = specRpm;
    }

    /**
     * @return the pearlVer
     */
    public String getPearlVer()
    {
        return pearlVer;
    }

    /**
     * @param pearlVer the pearlVer to set
     */
    public void setPearlVer(String pearlVer)
    {
        this.pearlVer = pearlVer;
    }

    /**
     * @return the release
     */
    public int getRelease()
    {
        return release;
    }

    /**
     * @param release the release to set
     */
    public void setRelease(int release)
    {
        this.release = release;
    }

    /**
     * @return the version
     */
    public String getVersion()
    {
        return version;
    }

    /**
     * @param version the version to set
     */
    public void setVersion(String version)
    {
        this.version = version;
    }
    
    /**
     * @return the rpmUrl
     */
    public String getRpmUrl()
    {
        return rpmUrl;
    }

    /**
     * @param rpmUrl the rpmUrl to set
     */
    public void setRpmUrl(String rpmUrl)
    {
        this.rpmUrl = rpmUrl;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("ProbeUpdate [probeId=").append(probeId)
                .append(", specRpm=").append(specRpm).append(", pearlVer=")
                .append(pearlVer).append(", version=").append(version)
                .append(", release=").append(release).append(", rpmUrl=")
                .append(rpmUrl).append("]");
        return builder.toString();
    }
}
