/**
 * 
 */
package com.fnic.pearl.scheduler.constant;

/**
 * 调度器支持的 URL 列表
 * 
 * @author HuHaiyang
 * @date 2013年7月17日
 */
public class SchedulerUrl {

	// 协议版本
	public static final String VER = "1.1";

	// 心跳
	public static final String HEARTBEAT = "/pearl/hb";

	// 登录
	public static final String LOGIN = "/pearl/login";

	// 退出
	public static final String LOGOUT = "/pearl/logout";

	// 心跳至 管理节点(调度器 -> 管理节点)
	public static final String HEARTBEAT_MANAGER = "/scheduler/hb";

	// 上报任务状态至 管理节点(调度器 -> 管理节点)
	public static final String TESTASK_STATUS_MANAGER = "/scheduler/taskstatus";

	// 登录管理节点
	public static final String SCHEDULER_LOGIN = "/scheduler/login";
	// public static final String SCHEDULER_LOGIN =
	// "/pearl/scheduler/loginscheduler";

	// 上报任务状态
	public static final String TESTASK_STATUS = "/pearl/testask/status";

	// 下发测量任务(管理节点 -> 调度器)
	public static final String TESTASK_ISSUE = "/pearl/task/issue";

	// 通用类型任务的下发
	public static final String TASK_ISSUE = "/task/issue";

	// 探针登录状态上报至 管理节点
	public static final String PROBE_LOGIN = "/probe/login";
	// 管理节点重启时，探针批量登录
	public static final String PROBE_BATCH_LOGIN = "/probe/batchLogin";

	// 探针登出状态上报至 管理节点
	public static final String PROBE_LOGOUT = "/probe/logout";

	// 版本在线升级
	public static final String VERSION_UPDATE = "/pearl/version/update";

	// 版本在线升级状态
	public static final String VERSION_UPDATE_STATUS = "/pearl/version/update/status";

	// 版本升级状态上报新接口 /pearl/version/update/status
	public static final String PROBE_UPDATE_STATUS = "/probe/update/status";

	// 任务控制
	public static final String TASK_CTRL = "/task/ctrl";

	// -------- 下面为 维护通道 的 URL ----------

	// 查看指定任务的详情
	public static final String M_TASK_INFO = "/pearl/scheduler/task/info";

	// 查看当前登录的探针列表
	public static final String M_PROBE_LIST = "/pearl/scheduler/probes";

	// 查看指定 探针标识 的探针信息
	// ?probeId=123
	public static final String M_PROBE_INFO = "/pearl/scheduler/probe";

	// 查看当前任务列表的统计信息
	public static final String M_TASK_STATS = "/pearl/scheduler/task/stats";

	// 通知 probe 在线升级 POST url:
	public static final String M_PROBE_UPDATE = "/pearl/scheduler/probe/update";

	// 查看调度器的版本信息
	public static final String M_SCHEDULER_VERSION = "/pearl/scheduler/version";

	/** 获取系统状态信息 **/
	public static final String MSG_INTERFACE_INFO = "/msg/interface/info";
}
