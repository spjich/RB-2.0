/**
 * 
 */
package com.fnic.pearl.scheduler.constant;

/**
 * 定义 scheduler 在 redis 中存储数据的 key format 
 * 
 * @author HuHaiyang
 * @date 2013年7月21日
 */
public class SchedulerRedisKey
{
    /**
     * 测试任务记录
     */
    public static final String TESTASK = "tt:task:%d";
    
    /**
     * 不同状态的探针-任务列表
     */
    public static final String TESTASK_STATUS_PROBE = "tt:sp:%d:%d";
    
    /**
     * 探针-任务状态
     */
    public static final String TESTASK_PROBE_TASK = "tt:pt:%d:%d";
    
    /**
     * 任务当前状态
     */
    public static final String TESTASK_TASK_STATUS = "tt:ts:%d";
    
    /**
     * 任务上一次执行时间
     */
    public static final String TESTASK_TASK_LASTIME = "tt:tlast:%d";
    
    /**
     * 任务当前执行数
     */
    public static final String TESTASK_TASK_EXECNT = "tt:texecnt:%d";
}
