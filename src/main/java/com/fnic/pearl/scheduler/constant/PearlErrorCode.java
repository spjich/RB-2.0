/**
 * 
 */
package com.fnic.pearl.scheduler.constant;

/**
 * 定义 Pearl 系统的错误码
 * 
 * @author HuHaiyang
 * @date 2013年12月26日
 */
public class PearlErrorCode
{
    public static final int SUCCESS = 0x0;
    
    public static final int SCHD_ERR_START = 0x02100000;
    
    //-------- 网络通信类错误 ------
    
    public static final int SCHD_ERR_NET_START = SCHD_ERR_START + 0x10000;
    
    //-------- 业务类错误 --------
    
    public static final int SCHD_ERR_SVC_START = SCHD_ERR_START + 0x20000;
    
    // 任务ID 无效
    public static final int SCHD_ERR_SVC_TASK_ID_INVALID = SCHD_ERR_SVC_START + 1;
    
    // probe id 无效
    public static final int SCHD_ERR_SVC_PROBE_ID_INVALID = SCHD_ERR_SVC_START + 2;
    
    // tool 无效
    public static final int SCHD_ERR_SVC_TOOL_INVALID = SCHD_ERR_SVC_START + 3;
    
    // run 运行方式 无效
    public static final int SCHD_ERR_SVC_RUN_INVALID = SCHD_ERR_SVC_START + 4;
    
    // 当前时间 大于 结束时间
    public static final int SCHD_ERR_SVC_INVALID_END_TIME = SCHD_ERR_SVC_START + 5;
    
    // 升级任务 ID 无效
    public static final int SCHD_ERR_SVC_UPD_ID_INVALID = SCHD_ERR_SVC_START + 6;
    
    // 更新版本号无效
    public static final int SCHD_ERR_SVC_UPD_VER_INVALID = SCHD_ERR_SVC_START + 7;
    
    // 任务类型 无效
    public static final int SCHD_ERR_SVC_TASK_TYPE_INVALID = SCHD_ERR_SVC_START + 8;
    
    // 模块标识无效
    public static final int SCHD_ERR_SVC_MOD_ID_INVALID = SCHD_ERR_SVC_START + 100;
    
    // 模块类型无效
    public static final int SCHD_ERR_SVC_MOD_TYPE_INVALID = SCHD_ERR_SVC_START + 100;
}
