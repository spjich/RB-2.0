/**
 * 
 */
package com.fnic.pearl.scheduler.constant;

/**
 * 任务控制操作类型
 * 
 * @author HuHaiyang
 * @date 2014年3月19日
 */
public class TaskCtrlOp
{
    public static final int PAUSE = 1;
    public static final int RESUME = 2;
    public static final int REMOVE = 3;
}
