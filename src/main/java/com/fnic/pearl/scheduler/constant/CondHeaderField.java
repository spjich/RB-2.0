/**
 * 
 */
package com.fnic.pearl.scheduler.constant;

/**
 * URL 中的 condition 名以及自定义 http header 名
 * 
 * @author HuHaiyang
 * @date 2013年7月16日
 */
public class CondHeaderField {
	
    /**
     * 协议版本
     */
	public static final String COND_VER = "v";

	/**
     * 探针标识
     */
	public static final String COND_PROBE_ID = "probeId";

	/**
     * 探针版本
     */
	public static final String COND_PROBE_VER = "probeVer";

	// MAC address
	public static final String COND_PROBE_MAC = "mac";

	// icare type
	public static final String COND_PROBE_ICARETYPE = "icareType";

	// ip
	public static final String COND_PROBE_IP = "ip";

	/**
     * 探针 serial
     */
	public static final String COND_PROBE_SERIAL = "serial";

	/**
     * 任务标识
     */
	public static final String COND_TASK_ID = "taskId";

	/**
     * 调度器节点标识
     */
	public static final String COND_SCHEDULER_ID = "schedulerId";

	/**
     * 调度器版本号
     */
	public static final String COND_SCHEDULER_VER = "schedulerVer";

	/**
     * 调度器-内网端口
     */
	public static final String SCHEDULER_INNER_PORT = "schedulerInnerPort";

	/**
     * 调度器-内网ip
     */
	public static final String SCHEDULER_INNER_IP = "schedulerInnerIp";

	/**
     * 调度器-端口
     */
	public static final String SCHEDULER_PORT = "schedulerPort";

	/**
     * 调度器-ip
     */
	public static final String SCHEDULER_IP = "schedulerIp";

    /**
     * 调度器域名
     */
    public static final String SCHEDULER_DOMAIN = "schedulerDomain";

	/**
     * 主要用于心跳响应中，用于指定消息体中的内容 的具体类型，如：下发测试任务 - testask
     */
	public static final String HB_BODY_TYPE = "X-Fnic-Pearl-HB-BodyType";

	/**
     * 指定消息内容是否加密 如果加密，则取值设为 1，否则设置为 0 默认不加密
     */
	public static final String BODY_ENCRYPT = "X-Fnic-Pearl-Encrypt";

	/**
     * HB_BODY_TYPE 支持的类型名称
     */
	public static final String HB_BTYPE_TESTASK = "testask";
	public static final String HB_BTYPE_PROBEUPDATE = "probeUpdate";
	public static final String HB_BTYPE_SESSION_KEY = "sessionKey";
	public static final String HB_BTYPE_VER_UPD = "versionUpdate";

	/**
     * 设置 cookie
     */
	public static final String HEADER_SET_COOKIE = "Set-Cookie";

	/**
	 * cookie
	 */
	public static final String HEADER_COOKIE = "Cookie";

	/**
	 * jsessionid
	 */
	public static final String SESSION_ID = "JSESSIONID";
}
