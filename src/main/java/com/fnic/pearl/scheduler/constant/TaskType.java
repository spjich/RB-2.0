/**
 * 
 */
package com.fnic.pearl.scheduler.constant;

/**
 * 任务类型
 * 
 * @author HuHaiyang
 * @date 2014/04/25
 */
public class TaskType
{
	public static final String TEST_TASK = "testask";
	public static final String PERF_MONI = "perf-moni";
}
