/**
 * 
 */
package com.fnic.pearl.scheduler.constant;

/**
 * 定义 测试任务状态
 * 
 * @author HuHaiyang
 * @date 2013年7月14日
 */
public final class TestTaskStatus
{
    public static final int S_UNKNOWN = -1;
    public static final int S_INIT = 0;
    public static final int S_NEW_TASK = 1;
    public static final int S_WISSUE = 2;
    public static final int S_ISSUED = 3;
    public static final int S_RUNNING = 4;
    public static final int S_EXEC_SUCC = 5;
    public static final int S_ISSUE_ES_SUCC = 6;
    public static final int S_CREATE_IDX_SUCC = 7;
    public static final int S_TASK_MOVING = 8;
    public static final int S_TASK_MOVE_SUCC = 9;
    public static final int S_ISSUE_REDIS_SUCC = 10;
    
    public static final int S_FAILED_TASK_PARSE = 101;
    public static final int S_FAILED_TASK_ISSUE = 102;
    public static final int S_FAILED_TASK_HANGUP = 103;
    public static final int S_FAILED_TASK_EXEC = 104;
    public static final int S_FAILED_RESULT_ISSUE = 105;
    public static final int S_FAILED_CREATE_IDX = 106;
    public static final int S_FAILED_TIMEOUT = 107;
    public static final int S_FAILED_MOVE = 108;
    public static final int S_FAILED_PROBE_LOGOUT = 109;
    public static final int S_FAILED_ISSUE_REDIS = 110;
    
    public static final int S_END = 1000;
    
    public static boolean isUnknown(int status)
    {
        if ((status >= S_INIT && status <= S_TASK_MOVE_SUCC)
                || (status >= S_FAILED_TASK_PARSE && status <= S_FAILED_MOVE))
        {
            return false;
        }
        
        return true;
    }
}
