/**
 * 
 */
package com.fnic.pearl.scheduler.constant;

/**
 * 模块类型定义
 * 
 * @author HuHaiyang
 * @date 2014年2月14日
 */
public class ModuleType
{
    public static final int SCHEDULER = 40;
    public static final int PROBE_X86 = 80;
    public static final int PROBE_MIPS = 81;
}
