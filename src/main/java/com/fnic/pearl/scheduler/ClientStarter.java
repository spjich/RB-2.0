/**
 * 
 */
package com.fnic.pearl.scheduler;

import org.apache.log4j.Logger;

import com.fnic.pearl.scheduler.mod.ActiveToManager;
import com.fnic.pearl.scheduler.mod.HeartBeatRunnable;

/**
 * 作为客户端启动的线程
 * 
 * @author HuHaiyang
 * @date 2013年7月15日
 */
public class ClientStarter implements Runnable
{
    private static Logger LOG = Logger.getLogger(ClientStarter.class);
    
    public void run()
    {
        LOG.info("start client ...");
        
        Thread thrdHeartBeat = new Thread(new HeartBeatRunnable(
                SchedulerConfig.getInstance().getManagerIp(), 
                SchedulerConfig.getInstance().getManagerPort()));
        thrdHeartBeat.start();
        
        Thread thrdATM = new Thread(new ActiveToManager());
        thrdATM.start();
        
        try
        {
            thrdHeartBeat.join();
            thrdATM.join();
        }
        catch (InterruptedException e)
        {
            LOG.error("clients exception: " + e.getMessage());
        }
    }
}
