package com.fnic.pearl.scheduler;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;

import com.fnic.pearl.scheduler.dao.SchedulerStore;
import com.fnic.pearl.scheduler.mod.TestTaskSchedulerHandler;
import com.fnic.pearl.scheduler.mod.TestTaskStatusHandler;
import com.fnic.pearl.scheduler.model.SchedulerVersion;

/**
 * Scheduler 主入口
 */
public class Scheduler 
{
    private static final Logger LOG = Logger.getLogger(Scheduler.class);
    
    private static final String OPTION_HELP = "help";
    private static final String OPTION_VERSION = "version";
    
    public static void main( String[] args ) throws InterruptedException
    {
        Options options = new Options();
        options.addOption(new Option(OPTION_HELP, "print this help info and exit."));
        options.addOption(new Option(OPTION_VERSION, "print scheduler version and exit."));
        
        CommandLineParser cmdParser = new BasicParser();
        try
        {
            CommandLine cmdLine = cmdParser.parse(options, args);
            if (cmdLine.hasOption(OPTION_HELP))
            {
                HelpFormatter helpFormatter = new HelpFormatter();
                helpFormatter.printHelp("scheduler", options);
                return ;
            }
            else if (cmdLine.hasOption(OPTION_VERSION))
            {
                System.out.println("scheduler version: " + SchedulerVersion.version);
                return ;
            }
        }
        catch (ParseException e1)
        {
            System.err.println("Parse command line option failed - " + e1.getMessage());
            return ;
        }
        
        LOG.info("------> version: " + SchedulerVersion.version + " <--------");
        
        // 加载配置
        try
        {
            SchedulerConfig.getInstance().load();
            LOG.info("scheduler config - " + SchedulerConfig.getInstance());
        }
        catch (ConfigurationException e)
        {
            LOG.error("load scheduler configuration failed: " + e.getMessage());
            return ;
        }
        
        // 启动存储服务
        if (!SchedulerStore.getInstance().init(
                SchedulerConfig.getInstance().isRedisEnable(),
                SchedulerConfig.getInstance().getRedisMasterIp(),
                SchedulerConfig.getInstance().getRedisMasterPort(),
                SchedulerConfig.getInstance().getRedisSlaveIp(),
                SchedulerConfig.getInstance().getRedisSlavePort(),
                SchedulerConfig.getInstance().getRedisMasterTimeout()))
        {
            LOG.error("init scheduler store failed!!!");
            return ;
        }
        
        // 作为服务端
        Thread thrdServer = new Thread(new ServerStarter());
        thrdServer.start();
        
        // 作为客户端
        Thread thrdClient = new Thread(new ClientStarter());
        thrdClient.start();
        
        // 启动各业务处理模块
        Thread thrdTTSHandler = new Thread(new TestTaskStatusHandler(
                SchedulerConfig.getInstance().getManagerIp(),
                SchedulerConfig.getInstance().getManagerPort()));
        thrdTTSHandler.start();
        
        // 启动 TestTask scheduler handler pool
        Thread thrdSchedulerHandler = new Thread(new TestTaskSchedulerHandler());
        thrdSchedulerHandler.start();
        
        try
        {
            thrdServer.join();
            thrdClient.join();
            thrdTTSHandler.join();
            thrdSchedulerHandler.join();
        }
        finally
        {
            SchedulerStore.getInstance().destory();
        }
    }
}
