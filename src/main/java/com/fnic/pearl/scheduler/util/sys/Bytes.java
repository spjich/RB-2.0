package com.fnic.pearl.scheduler.util.sys;

/**
 * 
 * @Title:
 * @Description:
 * @Author:吉
 * @Since:2016年5月3日
 * @Version:1.1.0
 */
public class Bytes {

	public static String substring(String src, int start_idx, int end_idx) {
		byte[] b = src.getBytes();
		String tgt = "";
		for (int i = start_idx; i <= end_idx; i++) {
			tgt += (char) b[i];
		}
		return tgt;
	}

}
