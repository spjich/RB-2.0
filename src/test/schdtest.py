#!/usr/bin/python

import httplib
import urllib
import json
from datetime import datetime
import time
import threading


SCHDULER_IP = "192.168.28.64"
SCHEDULER_PORT = 8083

TASK_CNT = 1000000
TASK_INTERVAL = 2

PROBES_ID = [200001]

TOOL_NAME = ["lft", "probed"]
TOOL_TPL = {"lft": 43, "probed": 32}
TOOL_PERIOD = {"lft": 10, "probed": 60}
TOOL_PARAMS = {"lft": ["192.168.28.110"], "probed": ["127.0.0.1", "60666", "4"]}
TOOL_EXECNT = {"lft": 5, "probed": 1}

class TaskIssuer(threading.Thread):
    def __init__(self, probeId):
        threading.Thread.__init__(self)
        self._probeId = probeId
        
    def run(self):
        cnt = 0
        for idx in range(100001, TASK_CNT+1):
            testask = [ { \
                    "taskId": idx*(self._probeId%len(PROBES_ID)+1), \
                    "tool": TOOL_NAME[idx%2], \
                    "run": 1, \
                    "startTime": "2013-8-2 23:50:50", \
                    "endTime": "2013-9-10 23:50:50", \
                    "count": TOOL_EXECNT[TOOL_NAME[idx%2]], \
                    "realtime": 1, \
                    "periodSec": TOOL_PERIOD[TOOL_NAME[idx%2]], \
                    "inTempId": TOOL_TPL[TOOL_NAME[idx%2]], \
                    "probe": self._probeId, \
                    "inTempParams": TOOL_PARAMS[TOOL_NAME[idx%2]]} ]
            
            conn = httplib.HTTPConnection(SCHDULER_IP, SCHEDULER_PORT)
            headers = {"Content-Type":"application/json"}
            conn.request("POST", \
                    "/pearl/testask/issue", \
                    json.dumps(testask), \
                    headers)
            conn.getresponse()
            
            cnt = cnt + 1
            if cnt % 10 == 0:
                print "[%d][%s] cnt: %d" % \
                    (self._probeId, datetime.now().strftime("%Y-%m-%d %H:%M:%S,%f"), cnt)
           
            time.sleep(TASK_INTERVAL)

if __name__ == '__main__':
    
    for probe_id in PROBES_ID:
        TaskIssuer(probe_id).start()
        
