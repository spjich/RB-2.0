/**
 * 
 */
package com.fnic.pearl.scheduler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import com.fnic.pearl.scheduler.action.ProbeTestTaskStatusServlet;
import com.fnic.pearl.scheduler.constant.CondHeaderField;
import com.fnic.pearl.scheduler.constant.SchedulerUrl;

import junit.framework.TestCase;

/**
 * 测试任务状态 相关测试用例
 * 
 * @author HuHaiyang
 * @date 2013年7月19日
 */
public class ProbeTestTaskStatusServletCase extends TestCase
{
    private static final String RIGHT_REQ_TESTASK_STATUS = "{"
            + "\"taskId\": 123, "
            + "\"probeId\": 873, "
            + "\"status\": 2"
            + "}";
    
    private ProbeTestTaskStatusServlet servletProbeTestTaskStatus;
    
    /**
     * @param name
     */
    public ProbeTestTaskStatusServletCase(String name)
    {
        super(name);
    }
    
    /**
     * @see junit.framework.TestCase#setUp()
     */
    protected void setUp() throws Exception
    {
        servletProbeTestTaskStatus = new ProbeTestTaskStatusServlet();
    }

    /**
     * @see junit.framework.TestCase#tearDown()
     */
    protected void tearDown() throws Exception
    {
        super.tearDown();
    }
    
    /**
     * 探针 正确 上报测试任务执行状态
     * 
     * @throws IOException ioex
     */
    public void test_servlet_succ() throws IOException
    {
        MockHttpServletRequest mockReq = new MockHttpServletRequest("POST", SchedulerUrl.TESTASK_STATUS);
        mockReq.addParameter(CondHeaderField.COND_PROBE_ID, "123");
        mockReq.setContent(RIGHT_REQ_TESTASK_STATUS.getBytes());
        MockHttpServletResponse mockResp = new MockHttpServletResponse();
        
        try
        {
            servletProbeTestTaskStatus.service(mockReq, mockResp);
        }
        catch (ServletException e)
        {
            e.printStackTrace();
            return ;
        }
        
        assertEquals(200, mockResp.getStatus());
    }
    
    /**
     * 探针上报测试任务执行状态，错误的METHOD
     * @throws IOException ioex
     */
    public void test_servlet_errmethod() throws IOException
    {
        MockHttpServletRequest mockReq = new MockHttpServletRequest("GET", SchedulerUrl.TESTASK_STATUS);
        mockReq.addParameter(CondHeaderField.COND_PROBE_ID, "123");
        mockReq.setContent(RIGHT_REQ_TESTASK_STATUS.getBytes());
        MockHttpServletResponse mockResp = new MockHttpServletResponse();
        
        try
        {
            servletProbeTestTaskStatus.service(mockReq, mockResp);
        }
        catch (ServletException e)
        {
            e.printStackTrace();
            return ;
        }
        
        assertEquals(HttpServletResponse.SC_BAD_REQUEST, mockResp.getStatus());
    }
    
    /**
     * 探针上报测试任务执行状态，未指定 probeId
     * @throws IOException ioex
     */
    public void test_servlet_noparam() throws IOException
    {
        MockHttpServletRequest mockReq = new MockHttpServletRequest("GET", SchedulerUrl.TESTASK_STATUS);
        mockReq.setContent(RIGHT_REQ_TESTASK_STATUS.getBytes());
        MockHttpServletResponse mockResp = new MockHttpServletResponse();
        
        try
        {
            servletProbeTestTaskStatus.service(mockReq, mockResp);
        }
        catch (ServletException e)
        {
            e.printStackTrace();
            return ;
        }
        
        assertEquals(HttpServletResponse.SC_BAD_REQUEST, mockResp.getStatus());
    }
}
